#!/bin/bash

BASEDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

MODULE="$1"
A="$2"
B="$3"
DIR="$4"
LOGFILE="$5"
HGMUSDIR="$6"
INSTANCENR="$7"
TIMEOUT="$8"

# echo "MODULE: $MODULE"
# echo "A: $A"
# echo "B: $B"
# echo "DIR: $DIR"
# echo "LOGFILE: $LOGFILE"
# echo "HGMUSDIR: $HGMUSDIR"

cat $MODULE | sed 's/OntologyID(Anonymous-2)#//g' > $MODULE.new

$HGMUSDIR/scripts/gen-sat-enc ll $MODULE.new $DIR $HGMUSDIR/tools/

grep "implies $A $B" $DIR/ll.zzz | cut -f1 -d' ' > $DIR/queries.q

$HGMUSDIR/scripts/create-wcnf ll $DIR $DIR/queries.q $DIR $HGMUSDIR/tools/ x2
S=`cat $DIR/queries.q`

# echo "S: $S"

$BASEDIR/runlim -t "$TIMEOUT" $HGMUSDIR/hgmus/hgmus -wmcs $DIR/ll.$S.x2.wcnf > $DIR/.out 2> $DIR/.out-err

cat  $DIR/.out

STATUS=`grep 'status:' $DIR/.out-err | cut -f2 -d':' | sed 's/\t//g' `
TIME=`grep 'time:' $DIR/.out-err | cut -f2 -d':' | sed 's/\t//g' | sed 's/ seconds//g'`
MUSES=`grep 'Number of MUSes' $DIR/.out | cut -f2 -d':' | sed 's/ //g'`
MODULESIZE=`cat $MODULE.new | wc -l`

echo "" >> $LOGFILE
echo "#$INSTANCENR $A-$B: $STATUS $TIME $MUSES $MODULESIZE" >> $LOGFILE
cat $DIR/.out-err >> $LOGFILE


# rm -f $DIR/*
# rm -f $DIR/.out
# rm -f $DIR/.out-err